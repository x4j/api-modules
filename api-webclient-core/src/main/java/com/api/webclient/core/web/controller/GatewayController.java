/**
 * 
 */
package com.api.webclient.core.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.webclient.core.gateway.GatewayDelegate;

/**
 * @author cmariano
 *
 */

@RestController
public class GatewayController { 
	
	@RequestMapping(value="/gateway/{id}/**", produces = { MediaType.APPLICATION_JSON_VALUE }, method = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
	protected String delegateToGateway(@PathVariable(value="id", required=true) String id, 
			HttpServletRequest request,  HttpServletResponse response) throws Exception {
		return GatewayDelegate.invoke(id, request, response);
	}
}
