package com.api.webclient.core;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

import com.api.webclient.core.interceptor.ExternalRequestInterceptor;
import com.api.webclient.core.web.controller.GatewayController;

@Configuration
@ComponentScan("com.api.webclient.core")
@EnableConfigurationProperties({ ResourceProperties.class })
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {
	
	@Autowired 
	private ExternalRequestInterceptor externalRequestInterceptor;
	
	@Autowired
	private ResourceProperties resourceProperties = new ResourceProperties();

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(externalRequestInterceptor).addPathPatterns("/**");
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    Integer cachePeriod = resourceProperties.getCachePeriod();

	    final String[] staticLocations = resourceProperties.getStaticLocations();
	    final String[] indexLocations  = new String[staticLocations.length];
	    
	    for (int i = 0; i < staticLocations.length; i++) {
	        indexLocations[i] = staticLocations[i] + "index.html";
	    }
	    registry.addResourceHandler(
	            "/**/*.css", "/**/*.html", "/**/*.js", "/**/*.json",
	            "/**/*.bmp", "/**/*.jpeg", "/**/*.jpg", "/**/*.png",
	            "/**/*.ttf", "/**/*.eot", "/**/*.svg", "/**/*.woff",
	            "/**/*.woff2"
	            )
	            .addResourceLocations(staticLocations)
	            .setCachePeriod(cachePeriod);

	    registry.addResourceHandler("/**")
	            .addResourceLocations(indexLocations)
	            .setCachePeriod(cachePeriod)
	            .resourceChain(true)
	            .addResolver(new PathResourceResolver() {
	                @Override
	                protected Resource getResource(String resourcePath,
	                        Resource location) throws IOException {
	                    return location.exists() && location.isReadable() ? location
	                            : null;
	                }
	            });
	}
	
	/**
	 * Instantiate GatewayController to handle request redirection to services.
	 * @return
	 */
	@Bean
	public GatewayController gatewayController() {
		return new GatewayController();
	}
}
