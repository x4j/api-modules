package com.api.webclient.core.rest;

import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 * Provides a single instance of RestTemplate for reuse
 * @author cmariano
 *
 */
public final class RestTemplateProvider {

	private static RestTemplate restTemplate;
	
	private RestTemplateProvider() {
		//Hide constructor
	}
	
	/**
	 * Provides an instance of rest template
	 * @return
	 */
	public static synchronized RestTemplate getInstance() {
		if (restTemplate == null) {
			ClientHttpRequestFactory requestFactory = new     
				      HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());
			restTemplate = new RestTemplate(requestFactory);
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			
			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				//Override to prevent throwing an exception non 200 response
				@Override
				protected boolean hasError(HttpStatus statusCode) {
					return false;
				}
			});
		}
		return restTemplate;
	}
}
