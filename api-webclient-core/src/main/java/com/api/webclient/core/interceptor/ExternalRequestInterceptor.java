package com.api.webclient.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * This interceptor handles requests to an external resource especially on microservies architecture.
 * <br>Requires a properties file (application.properties) to be defined which contains the mapping for
 * the api resource.
 * <br>i.e. gateway.api.url=http:api:9080/api
 * <br>~example above would redirect all /api request to the given url. 
 * @author cmariano
 *
 */
@Component
@Qualifier("externalRequestInterceptor")
public class ExternalRequestInterceptor implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		System.out.println("intercepted");
		return true;
	}
	
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// No implementation yet
	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// No implementation yet
	}
}
