package com.api.webclient.core.gateway.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.api.webclient.core.rest.RestTemplateProvider;

/**
 * Strategy for resolving different behaviors for HTTP verbs
 * @author cmariano
 *
 */
public enum HttpAction {

	GET {
		@Override
		ResponseEntity<String> doAction(String url, HttpEntity<?> request) throws Exception{
			return restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		}
	},
	
	POST {
		@Override
		ResponseEntity<String> doAction(String url, HttpEntity<?> request) throws Exception {
			return restTemplate.postForEntity(url, request, String.class);
		}
	},
	
	DELETE {
		@Override
		ResponseEntity<String> doAction(String url, HttpEntity<?> request) throws Exception{
			return restTemplate.exchange(url, HttpMethod.DELETE, request, String.class);
		}
	};
	
	private static RestTemplate restTemplate = RestTemplateProvider.getInstance();
	
	abstract ResponseEntity<String> doAction(String url, HttpEntity<?> request) throws Exception;
	
	public static HttpAction getAction(String method) {
		for (HttpAction action : HttpAction.values()) {
			if (action.toString().equalsIgnoreCase(method)) {
				return action;
			}
		}
		return null;
	}
}
