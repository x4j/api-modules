package com.api.webclient.core.gateway.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Pass through client which invokes an external rest service
 * @author cmariano
 *
 */
public class RestClient {

	public static String invoke(String url, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpAction action = HttpAction.getAction(request.getMethod());
		if (action != null) {
			String payload = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			HttpEntity<String> requestPayload = new HttpEntity<String>(payload, createHeader(request));
			ResponseEntity<String> entity = action.doAction(url, requestPayload);
			response.setStatus(entity.getStatusCodeValue());
			setAuthHeader(entity.getHeaders(), response);
			return entity.getBody();
		}
		return StringUtils.EMPTY;
	}
	
	private static MultiValueMap<String, String> createHeader(HttpServletRequest request) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (StringUtils.isNotBlank(authorization)) {
			headers.set(HttpHeaders.AUTHORIZATION, authorization);
		}

		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}

	private static void setAuthHeader(HttpHeaders httpHeaders, HttpServletResponse response) {
		List<String> authHeader = httpHeaders.get(HttpHeaders.AUTHORIZATION);
		if (! CollectionUtils.isEmpty(authHeader)) {
			response.setHeader(HttpHeaders.AUTHORIZATION, authHeader.stream() 
					.collect(Collectors.joining(",")));
		}
	}
}
