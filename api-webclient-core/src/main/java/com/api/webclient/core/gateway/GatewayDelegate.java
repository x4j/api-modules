package com.api.webclient.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;

import com.api.webclient.core.gateway.rest.RestClient;

/**
 * Handles the invocation of gateway.
 * @author cmariano
 *
 */
public final class GatewayDelegate {
	
	private static Logger logger = LoggerFactory.getLogger(GatewayDelegate.class);
	
	private static final String GATEWAY_PROP_URL = "url";
	
	/**
	 * Delegates the call to a microservice given the id
	 * @param id - of the gateway
	 * @param request - httprequest
	 * @param response - httpresponse
	 * @return the response
	 * @throws Exception - is thrown when an error occurs
	 */
	public static String invoke(String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			Gateway gateway = GatewayResolver.get(id);
			if (gateway != null) {
				return RestClient.invoke(resolveEndpoint(gateway, request), request, response);
			} 
		} catch (HttpClientErrorException e) {
			logger.error(e.getResponseBodyAsString());
			throw e;
		}
		return null;
	}
	
	private static String resolveEndpoint(Gateway gateway, HttpServletRequest request) {
		String uri = request.getRequestURI();
		int startingIndex = uri.indexOf(gateway.getName()) + gateway.getName().length();
		String partialUri = uri.substring(startingIndex); 
		String serviceEndpoint = gateway.getProperty(GATEWAY_PROP_URL);
		
		StringBuilder sb = new StringBuilder();
		sb.append(serviceEndpoint);
		sb.append(serviceEndpoint.endsWith("/") ? "" : "/");
		sb.append(partialUri);
		
		return sb.toString();
	}
}
