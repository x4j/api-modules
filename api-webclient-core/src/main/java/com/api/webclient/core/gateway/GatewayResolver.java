package com.api.webclient.core.gateway;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.api.common.util.PropertiesHelper;

/**
 * Resolves the service gateway.
 * @author cmariano
 *
 */
public final class GatewayResolver {
	
	private static final String PROPERTIES_FILE = "/application.properties";

	private static final String GATEWAY_PROPERTY_PREFIX = "gateway";
	
	private static Map<String, Gateway> gatewayRegistry = new HashMap<String, Gateway>();
	
	private static PropertiesHelper properties;
	
	static {
		try {
			properties = PropertiesHelper.getInstance(GatewayResolver.class, PROPERTIES_FILE);
			if (properties != null) {
				
				//Iterate through all properties and extract gateway properties
				for (Map.Entry<Object, Object> entrySet : properties.getEntrySet()) {
					String[] propKey = entrySet.getKey().toString().split("\\.");
					if (propKey.length == 3 && propKey[0].equals(GATEWAY_PROPERTY_PREFIX)) {
						String gatewayName = propKey[1];
						Gateway gateway = gatewayRegistry.get(gatewayName);
						
						if (gateway == null) {
							gatewayRegistry.put(gatewayName, new Gateway(gatewayName));
							gateway = gatewayRegistry.get(gatewayName);
						}
						
						gateway.setProperty(propKey[2], entrySet.getValue().toString());
					}
				}
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Gateway get(String gatewayName) {
		return gatewayRegistry.get(gatewayName);
	}
}
