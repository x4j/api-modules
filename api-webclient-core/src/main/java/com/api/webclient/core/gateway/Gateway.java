package com.api.webclient.core.gateway;

import java.util.HashMap;
import java.util.Map;

/**
 * Gateway object
 * @author cmariano
 *
 */
public class Gateway {

	private String name;
	
	private Map<String, String> property;

	public Gateway(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getProperty (String key) {
		return _getProperty().get(key);
	}

	public void setProperty (String key, String value) {
		_getProperty().put(key, value);
	}
	
	/**
	 * Encapsulates setting key and values to the properties.
	 * @return
	 */
	private Map<String, String> _getProperty() {
		if (property == null) {
			property = new HashMap<String, String>();
		}
		return property;
	}
}
