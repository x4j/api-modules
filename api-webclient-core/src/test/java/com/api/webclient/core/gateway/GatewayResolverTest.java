package com.api.webclient.core.gateway;

import org.junit.Assert;
import org.junit.Test;

import com.api.webclient.core.gateway.Gateway;
import com.api.webclient.core.gateway.GatewayResolver;


public class GatewayResolverTest {

	@Test
	public void testGetApi() throws Exception {
		Gateway gateway = GatewayResolver.get("api");
		Assert.assertNotNull(gateway);
		Assert.assertEquals("api", gateway.getName());
		Assert.assertEquals("http://localhost:9080", gateway.getProperty("url"));
		Assert.assertEquals("10000", gateway.getProperty("timeout"));
	}
	
	@Test
	public void testGetGoogleGateway() throws Exception {
		Gateway gateway = GatewayResolver.get("google");
		Assert.assertNotNull(gateway);
		Assert.assertEquals("google", gateway.getName());
		Assert.assertEquals("http://www.google.com/api", gateway.getProperty("url"));
	}

}
