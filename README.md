# API Modules
This is the parent pom for the modules that this application will be using

## Dependencies
- Java8 jdk
- Maven
- Eclipse IDE (or any Java IDE)

## Getting Started

```bash
git clone https://<your account>@bitbucket.org/x4j/api-modules.git
cd api-modules
mvn install 
```