package com.api.data.core.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.api.data.core.repository.BaseCrudRepository;

@RunWith(MockitoJUnitRunner.class)
public class AbstractServiceTest {

	@Mock
	private BaseCrudRepository<TestDomain, String> repository;
	
	@InjectMocks
	private TestService service; 
	
	@Test
	public void testFindById() throws Exception {
		String id = "id";
		service.findById(id);
		Mockito.verify(repository, Mockito.atLeastOnce()).findOne(id);
	}

	@Test
	public void testFindAll() throws Exception {
		service.findAll();
		Mockito.verify(repository, Mockito.atLeastOnce()).findAll();
	}

	@Test
	public void testCreateOrUpdateT() throws Exception {
		TestDomain domain = new TestDomain();
		service.createOrUpdate(domain);
		Mockito.verify(repository, Mockito.atLeastOnce()).save(domain);
	}

	@Test
	public void testDelete() throws Exception {
		String id = "id";
		service.delete(id);
		Mockito.verify(repository, Mockito.atLeastOnce()).delete(id); 
	}

	@Test
	public void testCount() throws Exception {
		service.count();
		Mockito.verify(repository, Mockito.atLeastOnce()).count();
	}

	@Test
	public void testExists() throws Exception {
		String id = "id";
		service.exists(id);
		Mockito.verify(repository, Mockito.atLeastOnce()).exists(id);
	} 
}
