package com.api.data.core.service;

import com.api.data.core.repository.BaseCrudRepository;
import com.api.data.core.service.AbstractService;

public class TestService extends AbstractService<TestDomain, String> {

	private BaseCrudRepository<TestDomain, String> repository;
	
	@Override
	protected BaseCrudRepository<TestDomain, String> getRepository() {
		return repository;
	} 

	public void setTestRepository(BaseCrudRepository<TestDomain, String> testRepository) {
		this.repository = testRepository;
	} 
}
