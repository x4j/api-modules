package com.api.data.core.service;

import java.io.Serializable;

import com.api.data.core.repository.BaseCrudRepository;

/**
 * Base class for services
 * @author cmariano
 *
 */
public abstract class AbstractService<T, ID extends Serializable> {
	
	protected abstract BaseCrudRepository<T, ID> getRepository();
	
	public T findById(ID id) {
		return getRepository().findOne(id);
	}
	
	public Iterable<T> findAll() {
		return getRepository().findAll();
	}
	
	public T createOrUpdate(T domain) {
		return getRepository().save(domain);
	}
	
	public Iterable<T> createOrUpdate(Iterable<T> collection) {
		return getRepository().save(collection);
	}
	
	public void delete(ID id) {
		getRepository().delete(id);
	}
	
	public long count() {
		return getRepository().count();
	}
	
	public boolean exists(ID id) {
		return getRepository().exists(id);
	}
 
}
