/**
 * 
 */
package com.api.data.core.service;

/**
 * Services that exposes behavior for the Repository.
 * @author cmariano
 *
 */
public interface Service<T, ID> {
	
	/**
	 * Returns the domain given the id
	 * @param id
	 * @return
	 */
	public T findById(ID id);
	
	/**
	 * Returns all the records
	 * @return
	 */
	public Iterable<T> findAll();
	
	/**
	 * Creates the domain if it doesn't exist, else just update based on the id.
	 * @param domain - the domain to be persisted
	 * @return the saved domain
	 */
	public T createOrUpdate(T domain);
	
	/**
	 * Persists the collection to the repository
	 * @param collection - the list of domain
	 * @return the persisted list.
	 */
	public Iterable<T> createOrUpdate(Iterable<T> collection);
	
	/**
	 * Deletes the domain with the given id from the repository
	 * @param id - of the deleted domain
	 */
	public void delete(ID id);
	
	/**
	 * Convenience method to return the total count for the domain on the repository.
	 * @return - the row count
	 */
	public long count();
	
	/**
	 * Returns if a domain of the given id exists.
	 * @param id of the domain
	 * @return true if exists, false otherwise
	 */
	public boolean exists(ID id);

}
