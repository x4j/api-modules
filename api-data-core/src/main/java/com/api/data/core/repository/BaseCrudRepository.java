
package com.api.data.core.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

/**
 * Base class for Repository
 * @author cmariano
 *
 * @param <T> - domain
 * @param <ID> - the id
 */
public interface BaseCrudRepository<T, ID extends Serializable> extends CrudRepository<T,ID> { 
	
}
