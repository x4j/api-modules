package com.api.rest.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.api.rest.core.web.controller.aop.ControllerAdviceHandler;

@Configuration
@ComponentScan(basePackages={"com.api.rest.core"})
public class WebMvcConfiguration  extends WebMvcConfigurationSupport {

	@Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("**/*.css", "**/*.js", "**/*.map", "*.html")
            	.addResourceLocations("classpath:META-INF/resources/")
            	.setCachePeriod(0);
    }
	
	@Bean
	public ControllerAdviceHandler controllerAdviceHandler() {
		return new ControllerAdviceHandler();
	}
}