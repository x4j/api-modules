/**
 * 
 */
package com.api.rest.core.web.controller.aop;

import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import com.api.rest.core.web.controller.dto.ResponseDto;

/**
 * Aspect for handling cross cutting concerns on controller.
 * 
 * @author cmariano
 *
 */
@ControllerAdvice
public class ControllerAdviceHandler {

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseDto processValidationError(MethodArgumentNotValidException ex, WebRequest request) {
		ServletWebRequest webRequest = ((ServletWebRequest) request);
		BindingResult result = ex.getBindingResult();
		FieldError error = result.getFieldError();
		String path = webRequest.getRequest().getRequestURI();
		String errMessage = resolveErrorMessage(error);
		
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		responseDto.setTimestamp(new Date().getTime());
		responseDto.setException(ex.getClass().getCanonicalName());
		responseDto.setMessage(errMessage);
		responseDto.setPath(path);
		return responseDto;
	}

	private String resolveErrorMessage(FieldError error) {
		if (error != null) {
			Locale currentLocale = LocaleContextHolder.getLocale();
			Object[] params = new Object[] { error.getField() };
			return messageSource.getMessage(error.getDefaultMessage(), params, currentLocale);
		}
		return null;
	}

}
