package com.api.rest.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class LocalizationConfiguration {

	private static final String DEFAULT_ENCODING = "UTF-8";
	
	private static final String MESSAGES_LOCATION = "classpath:messages/messages";

	@Bean(name = "messageSource")
	public ReloadableResourceBundleMessageSource messageSource() {
	  ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
	  messageBundle.setBasename(MESSAGES_LOCATION);
	  messageBundle.setDefaultEncoding(DEFAULT_ENCODING);
	  return messageBundle;
	}
}