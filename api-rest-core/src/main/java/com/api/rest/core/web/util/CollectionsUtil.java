package com.api.rest.core.web.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for collections
 * @author cmariano
 *
 */
public final class CollectionsUtil {

	private CollectionsUtil() {
		//Hide constructor
	}
	
	/**
	 * Converts an instance of iterable to a list.
	 * @param iterable
	 * @return
	 */
	public static <E> List<E> toList(Iterable<E> iterable) {
	    if(iterable instanceof List) {
	      return (List<E>) iterable;
	    }
	    ArrayList<E> list = new ArrayList<E>();
	    if(iterable != null) {
	      for(E e: iterable) {
	        list.add(e);
	      }
	    }
	    return list;
	  }
}
