# API Restdata Core
This is a Springboot based project template that exposes REST webservices backed up by persistent data.
Note that client applications still needs to provide specific implementation.

## Dependencies
- Java8 jdk
- Maven
- Eclipse IDE (or any Java IDE)