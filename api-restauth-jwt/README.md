# API Rest Authentication via JWT
This is a Springboot based project that secures endpoints using JWT

## Dependencies
- Java8 jdk
- Maven
- Eclipse IDE (or any Java IDE)