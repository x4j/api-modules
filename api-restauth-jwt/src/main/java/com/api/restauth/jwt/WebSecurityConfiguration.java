package com.api.restauth.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.api.restauth.jwt.filter.JWTAuthenticationFilter;
import com.api.restauth.jwt.filter.JWTLoginFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // disable caching
        http.headers().cacheControl();

        http.csrf().disable() // disable csrf for our requests.
	        .authorizeRequests()
	        .antMatchers("/").permitAll()
	        .antMatchers(HttpMethod.GET,"/webjars/**").permitAll()
	        .antMatchers(HttpMethod.GET,"/jsondoc-ui.html").permitAll()
	        .antMatchers(HttpMethod.GET,"/jsondoc/**").permitAll()
	        .antMatchers(HttpMethod.POST,"/login").permitAll()
	        .antMatchers(HttpMethod.POST,"/signup").permitAll()
	        .anyRequest().authenticated()
	        .and()
        
	        // We filter the /login requests
	        .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
	        
	        // And filter other requests to check the presence of JWT in header
	        .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        //Wire a service for looking up a user during login
        auth.userDetailsService(userDetailsServiceBean());
    }
    
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return userDetailsService;
    }
}
