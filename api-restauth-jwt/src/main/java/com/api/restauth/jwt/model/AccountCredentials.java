package com.api.restauth.jwt.model;

/**
 * Holds the credentials used to authenticate for the application
 * @author cmariano
 *
 */
public class AccountCredentials {

	private String username;

	private String password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String _username) {
		this.username = _username;
	}

	public void setPassword(String _password) {
		this.password = _password;
	}
}