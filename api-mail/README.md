# API Mail
This is a Springboot based project template that provides functionalities for mail functionalities, such as sending template email messages.

## Dependencies
- Java8 jdk
- Maven
- Eclipse IDE (or any Java IDE)

## Getting Started
Declare the following configuration in your application.properties, remember to adjust the values accordingly.

```
spring.mail.host=smtp.example.com
spring.mail.port=25 # SMTP server port
spring.mail.username= # Login used for authentication
spring.mail.password= # Password for the given login
spring.mail.protocol=smtp
spring.mail.defaultEncoding=UTF-8 # Default message encoding
```

## Sending email with a template
Templates could be any html template. These should be located under src/main/resources/templates.
