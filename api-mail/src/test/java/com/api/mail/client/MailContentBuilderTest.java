package com.api.mail.client;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.mail.client.MailContentBuilder;


@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:application.properties")
@SpringBootTest
public class MailContentBuilderTest {

	@Autowired
	private MailContentBuilder builder;
	
	@Test
	public void testBuild() throws Exception {
		Map<String, String> templateArgs = new HashMap<String, String>();
		templateArgs.put("message", "Test message content"); 
		String content = builder.build("mailTemplate", templateArgs);
		
		Assert.assertEquals("<span>Test message content</span>", content);
	}

}
