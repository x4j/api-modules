package com.api.mail.client;

import java.util.HashMap;
import java.util.Map;

import javax.mail.Message.RecipientType;
import javax.mail.internet.MimeMessage;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.mail.client.MailClient;
import com.api.mail.client.MailHeader;
import com.api.mail.client.MailTemplate;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;

@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:application.properties")
@SpringBootTest
public class MailClientTest {

	@Autowired
    private MailClient mailClient;
	
	private GreenMail smtpServer;
	
	@Before
    public void setUp() throws Exception {
        smtpServer = new GreenMail(ServerSetupTest.SMTP);
        smtpServer.start();
    }
 
    @After
    public void tearDown() throws Exception {
        smtpServer.stop();
    }
	
	@Test
	public void testSend() throws Exception {
		String emailText = "Test message content\r\n";
		String from = "john@greenmail.com";
		String subject = "Hello World";
		String to = "name@email.com";

		MailHeader header = new MailHeader();
		header.setFrom(from);
		header.setTo(to); ;
		header.setSubject(subject);
	    
	    mailClient.send(header, emailText);
	    
	    MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
	    Assert.assertEquals(1, receivedMessages.length); 
	    Assert.assertEquals(receivedMessages[0].getContent(), emailText);
	    Assert.assertEquals(receivedMessages[0].getFrom()[0].toString(), from);
	    Assert.assertEquals(receivedMessages[0].getSubject(), subject);
	    Assert.assertEquals(receivedMessages[0].getRecipients(RecipientType.TO)[0].toString(), to);
	}
	
	@Test
	public void testSendAsTemplate() throws Exception { 
		String from = "john@greenmail.com";
		String subject = "Hello World";
		String to = "name@email.com";

		MailHeader header = new MailHeader();
		header.setFrom(from);
		header.setTo(to); ;
		header.setSubject(subject);
	    
		Map<String, String> templateArgs = new HashMap<String, String>();
		templateArgs.put("message", "Test message content");
		MailTemplate template = new MailTemplate("mailTemplate", templateArgs); 
	    mailClient.send(header, template);
	    
	    MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
	    Assert.assertEquals(1, receivedMessages.length); 
	    Assert.assertEquals(receivedMessages[0].getContent(), "<span>Test message content</span>\r\n");
	    Assert.assertEquals(receivedMessages[0].getFrom()[0].toString(), from);
	    Assert.assertEquals(receivedMessages[0].getSubject(), subject);
	    Assert.assertEquals(receivedMessages[0].getRecipients(RecipientType.TO)[0].toString(), to);
	}
 

}
