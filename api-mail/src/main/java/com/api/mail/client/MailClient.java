package com.api.mail.client;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 * Serves as the client for for communicating to the mail server.
 * 
 * @author cmariano
 *
 */
@Service
public class MailClient {
	
	private static Logger logger = LoggerFactory.getLogger(MailClient.class);

	@Autowired
	private MailContentBuilder contentBuilder;
	
	@Autowired
	private JavaMailSender mailSender;
	
	/**
	 * Sends the message based on an instance of {@link MailHeader},and a template option.
	 * @param header - instance of {@link MailHeader} that defines the header of the mail such as TO, FROM, 
	 * @param template - instance of {@link MailTemplate} that defines this mail template options
	 */
	public void send(final MailHeader header, final MailTemplate template) {
		send(header, contentBuilder.build(template.getTemplateName(), template.getTemplateArgs()), false);
	}
	
	/**
	 * Sends the message based on an instance of {@link MailHeader},and a template option.
	 * @param header - instance of {@link MailHeader} that defines the header of the mail such as TO, FROM, 
	 * @param template - instance of {@link MailTemplate} that defines this mail template options
	 * @param sendAsHtml - true to send as html, false otherwise.
	 */
	public void send(final MailHeader header, final MailTemplate template, boolean sendAsHtml) {
		send(header, contentBuilder.build(template.getTemplateName(), template.getTemplateArgs()), sendAsHtml);
	} 
	
	/**
	 * Sends the message based on an instance of {@link MailHeader}, and the content
	 * @param header - instance of {@link MailHeader} that defines the header of the mail such as TO, FROM, 
	 * @param content - the email body/content
	 */
	public void send(final MailHeader header, final String content) {
		send(header, content, false);
	}
	
	/**
	 * Sends the message based on an instance of {@link MailHeader}, and the content
	 * @param header - instance of {@link MailHeader} that defines the header of the mail such as TO, FROM, 
	 * @param content - the email body/content
	 * @param sendAsHtml - true to send as html, false otherwise.
	 */
	public void send(final MailHeader header, final String content, boolean sendAsHtml) {
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(header.getFrom());
				messageHelper.setTo(header.getTo());
				messageHelper.setSubject(header.getSubject());
				messageHelper.setText(content, sendAsHtml); 
			} 
		}; 
		
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
			logger.error("Error sending mail: {}", e.getLocalizedMessage());
		}
	}
}
