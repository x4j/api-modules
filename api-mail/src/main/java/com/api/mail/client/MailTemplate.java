package com.api.mail.client;

import java.util.HashMap;
import java.util.Map;

public class MailTemplate {
	
	private String templateName;
	
	private Map<String, String> templateArgs;
	
	/**
	 * Initialize the email template.
	 * @param templateName - the template name to generate the email body.
	 * @param templateArgs - arguments that will be used to extrapolate the placeholders on the template.
	 */
	public MailTemplate(String templateName , Map<String, String> templateArgs) {
		this.templateName = templateName;
		this.templateArgs = templateArgs;
	}

	public String getTemplateName() {
		return templateName;
	}

	public Map<String, String> getTemplateArgs() {
		if (templateArgs == null) {
			templateArgs = new HashMap<String, String>();
		}
		return templateArgs;
	} 
}
