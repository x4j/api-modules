package com.api.mail.client;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Allows to build a rich content message content by using a template
 * @author cmariano
 *
 */
@Service
public class MailContentBuilder {

	private TemplateEngine templateEngine;
	 
    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }
 
    /**
     * Builds the content based on the given template and its arguments binding.
     * @param templateName - name of the template (i.e. This should be an html file, located under src/main/resources/templates)
     * @param templateArgs - key-value pair which binds to the template's placeholders.
     * @return
     */
    public String build(String templateName, Map<String, String> templateArgs) {
        Context context = new Context();
        for (Map.Entry<String, String> tag : templateArgs.entrySet()) {
        	context.setVariable(tag.getKey(), tag.getValue());
		}
        return templateEngine.process(templateName, context);
    }
}
