package com.api.mail.client;

public class MailHeader {

	private String from;
	
	private String to;
	
	private String subject; 

	public String getFrom() {
		return from;
	}

	/**
	 * Sets the address to send the message to.
	 * @param from email address
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	/**
	 * Sets the address from whose account the message is being sent from.
	 * @param to email address
	 */
	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject of the email.
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	} 
}
