package com.api.common.util;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PropertiesHelperTest {

	private PropertiesHelper properties;
	
	@Before
	public void setup() {
		try {
			properties = PropertiesHelper.getInstance(PropertiesHelperTest.class, "application.properties");
			Assert.assertNotNull(properties);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testGet() {
		Assert.assertEquals(properties.getProperty("application.name"), "api-common-util");
		Assert.assertEquals(properties.getProperty("application.version"), "0.1.SNAPSHOT"); 
	} 
	
	@Test
	public void testGetEntrySet() {
		Set<Entry<Object, Object>> entrySet = properties.getEntrySet();
		Assert.assertNotNull(entrySet);
		Assert.assertTrue(entrySet.size() == 2);
		
		for (Entry<Object, Object> entry : entrySet) {
			if (entry.equals("application.name")) {
				Assert.assertEquals("api-common-util", entry.getValue());
			}
			if (entry.equals("application.version")) {
				Assert.assertEquals("0.1.SNAPSHOT", entry.getValue());
			}
		}
	}
}
