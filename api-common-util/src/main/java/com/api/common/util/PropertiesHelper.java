/**
 * 
 */
package com.api.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * Decorator class for extending functionalities for {@link Properties}
 * @author cmariano
 *
 */
public class PropertiesHelper {

	private static java.util.Properties props;
	
	private static PropertiesHelper instance;
	
	/**
	 * Gets an instance of {@link PropertiesHelper}
	 * @param clazz - class which invokes this method
	 * @param propertiesFile - the name of the properties file, relative to the classpath location.
	 * @return - instance of {@link PropertiesHelper}
	 * @throws IOException
	 */
	public static synchronized PropertiesHelper getInstance(Class<?> clazz, String propertiesFile) throws IOException {
			
		if(propertiesFile != null && ! propertiesFile.startsWith("/")) {
			StringBuilder sb = new StringBuilder("/");
			propertiesFile = sb.append(propertiesFile).toString(); 
		}
		
		if (instance == null) {
			instance = new PropertiesHelper();
			props = new java.util.Properties();
		}
		
		InputStream inputStream = clazz.getResourceAsStream(propertiesFile);
		
		if (inputStream == null) {
			throw new IOException("Properties file not found.");
		} 
		
		props.load(inputStream);
		return instance;
	}

	
	/**
	 * Returns the value of the given key based on the properties file.
	 * @param propKey
	 * @return
	 */
	public String getProperty(String propKey) {
		return props.getProperty(propKey);
	} 
	
	/**
	 * Returns all the properties as a set.
	 * @param 
	 * @return
	 */
	public Set<Entry<Object, Object>> getEntrySet() {
		return props.entrySet();
	}
}
