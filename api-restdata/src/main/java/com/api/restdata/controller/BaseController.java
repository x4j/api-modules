/**
 * 
 */
package com.api.restdata.controller;

import java.util.List;

import org.modelmapper.ModelMapper;

import com.api.data.core.service.Service;
import com.api.rest.core.web.util.CollectionsUtil;

/**
 * @author cmariano
 *
 */
public abstract class BaseController<T, ID, DTO> { 
	
	private static ModelMapper modelMapper = new ModelMapper(); 

	protected abstract Service<T, ID> getService();
	
	/**
	 * Gets the resource by ID
	 * @param id
	 * @return
	 */
	protected T getResourceById(ID id) {
		return getService().findById(id);
	}
	
	/**
	 * Gets all the resources
	 * @return
	 */
	protected List<T> getResourceAll() {
		return CollectionsUtil.toList(getService().findAll());
	}
	
	/**
	 * Adds or updates a resource
	 * @param domain
	 * @return
	 */
	protected T addOrUpdateResource(T domain) {
		return getService().createOrUpdate(domain);
	}
	
	/**
	 * Adds or updates a collection of resource.
	 * @param collection
	 * @return
	 */
	protected List<T> addOrUpdateResource(Iterable<T> collection) {
		return CollectionsUtil.toList(getService().createOrUpdate(collection));
	}
	
	/**
	 * Deletes a resource
	 * @param id
	 */
	protected void deleteResource(ID id) {
		getService().delete(id);
	}
	
	protected DTO convertToDto(T entity, Class<DTO> dtoClass) {
	    return modelMapper.map(entity, dtoClass);
	}
	
	protected T convertToEntity(DTO dto, Class<T> entityClass) {
		return modelMapper.map(dto, entityClass);
	}
}
